
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS dotnet-build
WORKDIR /src

COPY ./service/*sln ./
COPY ./service/Microsoft.DSX.ProjectTemplate.API/*.csproj ./Microsoft.DSX.ProjectTemplate.API/
COPY ./service/Microsoft.DSX.ProjectTemplate.Command/*.csproj ./Microsoft.DSX.ProjectTemplate.Command/
COPY ./service/Microsoft.DSX.ProjectTemplate.Data/*.csproj ./Microsoft.DSX.ProjectTemplate.Data/
COPY ./service/Microsoft.DSX.ProjectTemplate.Test/*.csproj ./Microsoft.DSX.ProjectTemplate.Test/

RUN dotnet restore

COPY ./service/Microsoft.DSX.ProjectTemplate.API/ ./Microsoft.DSX.ProjectTemplate.API/
COPY ./service/Microsoft.DSX.ProjectTemplate.Command/ ./Microsoft.DSX.ProjectTemplate.Command/
COPY ./service/Microsoft.DSX.ProjectTemplate.Data/ ./Microsoft.DSX.ProjectTemplate.Data/
COPY ./service/Microsoft.DSX.ProjectTemplate.Test/ ./Microsoft.DSX.ProjectTemplate.Test/

WORKDIR ./Microsoft.DSX.ProjectTemplate.API/
RUN dotnet publish -c Release -o /publish

WORKDIR ../Microsoft.DSX.ProjectTemplate.Command/
RUN dotnet publish -c Release -o /publish

WORKDIR ../Microsoft.DSX.ProjectTemplate.Data/
RUN dotnet publish -c Release -o /publish

WORKDIR ../Microsoft.DSX.ProjectTemplate.Test/
RUN dotnet publish -c Release -o /publish

FROM node:19 AS node-builder
WORKDIR /node
COPY ./client /node
RUN npm install
RUN npm run build

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS final
WORKDIR /app
RUN mkdir /app/wwwroot
COPY --from=dotnet-build /publish ./
COPY --from=node-builder /node/build ./wwwroot




